<?php

include_once( dirname(__FILE__) . '/Cfg.class.php');
include_once( dirname(__FILE__) . '/DB.class.php');

mb_internal_encoding(Cfg::get_param('encoding'));
ini_set('default_charset', Cfg::get_param('encoding'));

header("Content-type: text/html; charset=" . Cfg::get_param('html_header_charset'));

$menu_home =
'<li>
	<a href="#popup" data-icon="arrow-u" data-role="button" data-rel="dialog" data-theme="a">
		<span style="margin-left: -20px">Acerca de</span>
	</a>
</li>';

$menu_inner = 
'<li>
	<a data-theme="a" data-icon="back"  data-rel="back" href="##HREF_VOLVER##" data-theme="d">
		Volver
	</a>
</li>
<li>
	<a  data-icon="info" data-rel="dialog" href="#bibliografia" data-theme="a">
		Bibliografia
	</a>
</li>';

$footer = 
'<div data-role="footer"  data-theme="f">
	<center>
		<a href="#terminos" data-icon="arrow-u" data-role="button" data-rel="dialog" data-theme="a">
			<span style="margin-left: -20px">T�rminos y condiciones</span>
		</a>
	</center>
</div>';

$title_home = '<li data-role="list-divider" role="heading" class="ui-li ui-li-divider ui-bar-c ui-corner-top">Seleccione tipo de profilaxis</li>';

$template = 
'<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="iso-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ProfiPro</title>		
		<link rel="stylesheet" href="css/jquery.mobile-1.3.1.min.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.mobile-1.3.1.min.js"></script>
	</head>
	<body>

		<div id="initial-screen" data-role="page">
			<div data-role="header" data-theme="c" >
				<h1>ProfiPro</h1>
				<div data-role="navbar" data-iconpos="left" >
					<ul >
						##HEADER##
					</ul>
				</div>
			</div>
			<div id="lista" data-role="content">
				<ul data-role="listview" data-inset="true" data-theme="a">
					##TITLE_CONTENT##
					##CONTENT_LIs##
				</ul>
			</div>
			##FOOTER##
		</div>

		##POPUPS##

	</body>
</html>';

/*
$popup_template = ' 
	<div data-role="page" id="popup_##CAT_ID##">
	
		<div data-role="header" data-theme="a">
			<h1>##HEADER##</h1>
		</div><!-- /header -->
	
		<div data-role="content" data-theme="d">	
			<h2></h2>
			<p>##CONTENT##</p>	
		</div><!-- /content -->
		
		<div data-role="footer" style="display:none">
			<h4></h4>
		</div><!-- /footer -->
	</div><!-- /page popup -->		
'; 
*/

$popup_template  = 
'<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="iso-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ProfiPro</title>		
		<link rel="stylesheet" href="css/jquery.mobile-1.3.1.min.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.mobile-1.3.1.min.js"></script>
	</head>
	<body>

	<div data-role="dialog">
	
		<div data-role="header" data-theme="d">
			<h1>##HEADER##</h1>

		</div>

		<div data-role="content" data-theme="c">
			##CONTENT## 
		</div>
	</div>
	
	</body>
</html>'; 

function search_nodes($parent_id = 0)
{
	global $template, $title_home, $menu_home, $menu_inner, $footer, $popup_template; 
	$list = DB::query_list("SELECT * FROM categoria WHERE parent_id = $parent_id ORDER BY cat_id");

	$buffer = $template ; 
	
	$file_name = 'page_' . $parent_id . '.html';
	$dir_pages = Cfg::get_param('dir_pages'); 
	$file_path = $dir_pages . $file_name; 

	if ( file_exists($file_path)) unlink( $file_path); // borro el archivo si ya existe 

	if ($parent_id == 0 )	
	{
		$buffer = str_replace('##HEADER##', $menu_home, $buffer); 
		$buffer = str_replace('##TITLE_CONTENT##', $title_home, $buffer);
		$buffer = str_replace('##FOOTER##', $footer, $buffer);
	}
	else 
	{
		$menu_inner_tmp = str_replace('##HREF_VOLVER##', $file_name, $menu_inner);

		$buffer = str_replace('##HEADER##', $menu_inner_tmp, $buffer); 
		$buffer = str_replace('##TITLE_CONTENT##', $title_home, $buffer);			
		$buffer = str_replace('##FOOTER##', '', $buffer);
	}

	$lis = '';
	$popups = '';
	for ( $i = 0 ; $i < count ($list) ; $i++ )
	{
		$rec = $list[$i]; 
		if ( trim($rec->texto) == '' )
		{
			$lis .= '<li data-icon="false"><a href="page_' . $rec->cat_id . '.html" data-transition="slide" data-ajax="true">' . $rec->nombre . '</a></li>'; 
		} else 
		{
			$lis .= '<li data-icon="false"><a href="#popup_' . $rec->cat_id . '" data-rel="dialog"  data-ajax="true" data-transition="pop" >' . $rec->nombre . '</a></li>';

			$popup =  str_replace('##CAT_ID##', $rec->cat_id, $popup_template);
			$popup =  str_replace('##HEADER##', $rec->nombre, $popup);
			$popup =  str_replace('##CONTENT##', $rec->texto, $popup);
			$popups .= $popup; 
		}
	}

	$buffer = str_replace('##CONTENT_LIs##', $lis, $buffer);
	$buffer = str_replace('##POPUPS##', $popups, $buffer);

	file_put_contents($file_path, $buffer); 
}

function build_tree () 
{
	$list = DB::query_list("SELECT DISTINCT parent_id FROM categoria  ORDER BY cat_id");
	
	for ( $i  = 0 ; $i < count($list) ; ++$i)
	{
		$rec = $list[$i]; 
		echo "CREANDO NODO : {$rec->parent_id} <BR />" ; 
		search_nodes($rec->parent_id);
	}
}

build_tree(); 
