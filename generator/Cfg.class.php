<?php
/**
 * Encapsula los parámetros de configuracion del sitio. 
 * Hoy en día se setean los paramámetros de manera manual en el constructor para no tener que recurrir a variables globales, pero 
 * la idea es que más adelante haya un archivo de configuración en el DocumentRoot de donde se tomen los datos. 
 * 
 * @author DAN
 *
 */
class Cfg
{
	public static $_instance; 
	public $settings;

	public static function get_instance()
	{
		if ( ! isset(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function __construct()
	{
		$this->init();
	}

	public function init()
	{
		$cfg = array () ; 

		$files = $this->getConfigFilesList();
		foreach ($files as $file) 
		{
			include_once( $file ); 
		}

		foreach ($cfg as $key => $val) 
		{
			$this->set($key, $val);
		}
	
	}

	private function getConfigFilesList()
	{
		$files = array (); 
		$dir = dirname(__FILE__) . '/_cfg/'; 

		if ( is_dir($dir)) 
		{
		    if ($dh = opendir($dir)) 
		    {
				while (($file = readdir($dh)) !== false) 
				{
					if ( ! is_dir($dir . $file)) 
						$files[] = $dir . $file; 
				}
				closedir($dh);

				sort($files); 
		    }
		}
		return $files;
	}
	/** 
	 * Acceso directo a los parametros, solo para agilizar el codigo y evitar el Cfg::get_instance()->get(...)
	 * @param string $name
	 */
	public static function get_param($name)
	{
		return self::get_instance()->get($name);
	}
	
	public function set($name, $value)
	{
		$this->settings[$name] = $value;
	}

	public function get($name)
	{
		if (!isset($this->settings[$name])) return null;
		return $this->settings[$name]; 
	}
}