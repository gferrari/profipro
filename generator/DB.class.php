<?php 
class DB 
{
	public static $_instance ; 
	private $_conn;
	private $_stmt;

	/**
	 * Singleton: trae una instancia de la clase
	 */
	public static function get_instance() 
	{
		if (!isset(self::$_instance))
		{
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public static function get_conn()
	{
		return self::get_instance()->_conn;
	}

	public static function get_stmt()
	{
		return self::get_instance()->_stmt;
	}

	public function __construct()
	{
		$dsn 		= Cfg::get_param('db_dsn');
		$username 	= Cfg::get_param('db_username');
		$passwd 	= Cfg::get_param('db_password');
		$driver_opt	= Cfg::get_param('db_driver_options');

		try 
		{
			$this->_conn = new PDO($dsn, $username, $passwd, $driver_opt);
		} catch (PDOException $e) {
		    die( $e->getMessage() );
		}
	}

	/**
	 * Ejecuta una sentencia SQL y devuelve el numero de registros afectados. Si ocurrio un error devuelve false. 
	 * Para corroborar si hubo error NO se puede usar ( ! DB::exec (...) ) porque si la función devuelve 0 php la interpreta como error. 
	 * Por lo tanto hay que chequear de la siguiente manera  ( false === DB::exec('...') )
	 * @param string $statement
	 */
	public static function exec($statement)
	{
		$conn = self::get_instance()->get_conn();
		return $conn->exec($statement);
	}

	public static function last_insert_id($name = null)
	{
		$conn = self::get_instance()->get_conn();
		return $conn->lastInsertId($name);
	}

	/**
	 * Ejecuta una sentencia SQL y devuelve un PDOStatement.
	 * Si se le pasa el segundo $input_parameters y es un array entonces se asume que es una consulta "preparada", es decir que se le pasan valores 
	 * por medio de $input_parameters que serán reemplazados a la hora de ejecutar la consulta. Ej: 
	 * 
	 * DB::query("SELECT * FROM tabla WHERE    campo1 = ? " , array ('valor_campo_1')
	 * 
	 * @param string $statement Sentencia SQL normal. 
	 * @param array $input_parameters
	 * @param array $driver_options
	 * 
	 * @return PDOStatement
	 */
	public static function query($statement, $input_parameters = null, $driver_options = array())
	{
		if ( ! is_null($input_parameters) && is_array($input_parameters)) 
		{
			return self::prepare_execute($statement, $input_parameters, $driver_options);
		} else 
		{
			$conn = self::get_instance()->get_conn();
			return $conn->query($statement);
		}
	}

	/**
	 * Devuelve el primer registro devuelto por el query o false si ocurrió un error.
	 * @param string $statement
	 * @param array $input_parameters
	 * @param array $driver_options
	 * @param int $fetch_style
	 * 
	 * @return PDORow
	 */
	public static function query_single($statement, $input_parameters = null, $driver_options = array(), $fetch_style = PDO::FETCH_OBJ)
	{
		$rs = DB::query($statement, $input_parameters, $driver_options, $fetch_style);

		if ( ! $rs ) return false; 

		$row = $rs->fetch($fetch_style);
		$rs->closeCursor();
		return $row;
	}

	/**
	 * Devuelve un array con todos los registros devueltos por el query o un array vacio si la consulta dió error. 
	 * IMPORTANTE: Si bien agiliza el código, no es recomendable utilizar esta función ya que se pasan por alto los errores. 
	 * @param string $statement
	 * @param array $input_parameters
	 * @param array $driver_options
	 * @param int $fetch_style
	 * 
	 * @return array
	 */
	public static function query_list($statement, $input_parameters = null, $driver_options = array(), $fetch_style = PDO::FETCH_OBJ)
	{
		$rs = DB::query($statement, $input_parameters, $driver_options);

		if ( ! $rs ) return array (); 

		return $rs->fetchAll($fetch_style);
	}

	###########################################################################################################################
	### PREPARED STATEMENTS (No usar de manera directa, usar en cambio la función DB::query con el parametro $input_parameters)
	###########################################################################################################################
	
	public static function prepare($statement, $driver_options = array())
	{
		$conn = self::get_instance()->get_conn();

		self::get_instance()->_stmt = null; 
		self::get_instance()->_stmt = $conn->prepare($statement, $driver_options);
	}
	
	/* sin testear 
	public static function bindParam($parameter, &$variable, $data_type = PDO::PARAM_STR, $length = null, $driver_options = array())
	{
		$conn = self::get_instance()->get_conn();

		$stmt = self::get_instance()->get_stmt();
		if (! $stmt instanceof PDOStatement) throw new Exception(__METHOD__ . ": stmt is not a valid PDOStatement");

		$stmt->bindParam($parameter, $variable, $data_type, $length, $driver_options);
	} */

	public static function bind_value($parameter, $variable, $data_type = PDO::PARAM_STR)
	{
		$conn = self::get_instance()->get_conn();
		$stmt = self::get_instance()->get_stmt();
		if (! $stmt instanceof PDOStatement) throw new Exception(__METHOD__ . ": stmt is not a valid PDOStatement");

		$stmt->bindValue($parameter, $variable, $data_type);
	}

	public static function execute ($input_parameters = null) 
	{
		$stmt = self::get_instance()->get_stmt();
		if (! $stmt instanceof PDOStatement) throw new Exception(__METHOD__ . ": stmt is not a valid PDOStatement");

		$ok = $stmt->execute($input_parameters);

		if ( ! $ok ) return false;

		return $stmt; 
	}

	public static function prepare_execute($statement, $input_parameters = null, $driver_options = array())
	{
		self::prepare($statement, $driver_options);
		return self::execute($input_parameters);
	}

	###########################
	### END PREPARED STATEMENTS 
	###########################
	
	################
	### TRANSACTIONS
	################

	public static function begin()
	{
		$conn = self::get_instance()->get_conn();
		$conn->beginTransaction();
	}
	
	public static function commit()
	{
		$conn = self::get_instance()->get_conn();
		$conn->commit();
	}
	
	public static function rollback()
	{
		$conn = self::get_instance()->get_conn();
		$conn->rollBack();
	}

	#######################
	### END OF TRANSACTIONS
	#######################
}