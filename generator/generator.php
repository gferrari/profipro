<?php

include_once( dirname(__FILE__) . '/Cfg.class.php');
include_once( dirname(__FILE__) . '/DB.class.php');

mb_internal_encoding(Cfg::get_param('encoding'));
ini_set('default_charset', Cfg::get_param('encoding'));

header("Content-type: text/html; charset=" . Cfg::get_param('html_header_charset'));

$menu_home =
'<li>
	<a href="acercade.html" data-icon="arrow-u" data-role="button" data-rel="dialog"  data-transition="none" data-theme="a">
		<span style="margin-left: -20px">Acerca de</span>
	</a>
</li>';

$menu_inner = 
'<li>
	<a data-theme="a" data-icon="back"  data-rel="back" href="##HREF_VOLVER##" data-theme="d">
		Volver
	</a>
</li>
<li>
	<a  data-icon="info" data-rel="dialog"  data-transition="none" href="biblio_##BIBLIO_CAT_ID##.html" data-theme="a">
		Bibliografía
	</a>
</li>';

$footer = 
'<div data-role="footer"  data-theme="f">
	<center>
		<a href="agreement_page.html" data-icon="arrow-u" data-role="button" data-rel="dialog"  data-transition="none" data-theme="a">
			<span style="margin-left: -20px">Términos y condiciones</span>
		</a>
	</center>
</div>';

$title_home = '<li data-role="list-divider" role="heading" class="ui-li ui-li-divider ui-bar-c ui-corner-top">Seleccione tipo de profilaxis</li>';

$template = 
'<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="iso-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ProfiPro</title>		

		<link rel="stylesheet" href="css/jquery.mobile-1.3.1.min.css" />
		<link rel="stylesheet" href="css/custom.css" />
		
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.mobile-1.3.1.min.js"></script>		
		<script type="text/javascript" >
		$("#exit").live("tap", function() {
		    showConfirm();
		});

		function showConfirm() {
	      navigator.notification.confirm(
	            "Do you really want to exit?",  // message
	            exitFromApp,              // callback to invoke with index of button pressed
	            "Exit",            // title
	            "Cancel,OK"         // buttonLabels
	        );
		 }
    
	    function exitFromApp(buttonIndex) {
	      if (buttonIndex==2){
	       navigator.app.exitApp();
	    	}
		}	    	


		</script>
	</head>
	<body>

		<div id="initial-screen" data-role="page">
			<div data-role="header" data-theme="c" >
				##HOME_BTN##
				<h1>##MAIN_TITLE##</h1>
				##EXIT_BTN##
				<div data-role="navbar" data-iconpos="left" >
					<ul >
						##HEADER##
					</ul>
				</div>
			</div>
			<div id="lista" data-role="content">
				<ul data-role="listview" data-inset="true" data-theme="a">
					##TITLE_CONTENT##
					##CONTENT_LIs##
				</ul>
			</div>
			##FOOTER##
		</div>
		
		
 	</body>
</html>';

$popup_template  = 
'<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="iso-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ProfiPro</title>		
		<link rel="stylesheet" href="css/jquery.mobile-1.3.1.min.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.mobile-1.3.1.min.js"></script>
	</head>
	<body>

	<div data-role="dialog">
	
		<div data-role="header" data-theme="a">
			<h1>##HEADER##</h1>

		</div>

		<div data-role="content" data-theme="c">
			##CONTENT## 
		</div>
	</div>
	

	</body>
</html>'; 

function search_nodes($parent_id = 0)
{
	global $template, $title_home, $menu_home, $menu_inner, $footer, $popup_template; 
	$list = DB::query_list("SELECT * FROM categoria WHERE parent_id = $parent_id ORDER BY nombre");

	$buffer = $template ; 
	
	$file_name = 'page_' . $parent_id . '.html';
	$dir_pages = Cfg::get_param('dir_pages'); 
	$file_path = $dir_pages . $file_name; 

	if ( file_exists($file_path)) unlink( $file_path); // borro el archivo si ya existe 

	if ($parent_id == 0 )	
	{
		$buffer = str_replace('##MAIN_TITLE##', 'ProfiPro', $buffer);
		$buffer = str_replace('##HOME_BTN##', '', $buffer); 
		$buffer = str_replace('##EXIT_BTN##', '<a id="exit" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Home</a>', $buffer);		
		$buffer = str_replace('##HEADER##', $menu_home, $buffer); 
		$buffer = str_replace('##TITLE_CONTENT##', $title_home, $buffer);
		$buffer = str_replace('##FOOTER##', $footer, $buffer);
	}
	else 
	{
		$parent = DB::query_single("SELECT * FROM categoria WHERE cat_id = $parent_id");
		$menu_inner_tmp = str_replace('##HREF_VOLVER##', $file_name, $menu_inner);

		$buffer = str_replace('##MAIN_TITLE##', $parent->nombre, $buffer);
		$buffer = str_replace('##HOME_BTN##', '<a href="page_0.html"  data-transition="none" data-role="button" data-icon="home" data-iconpos="notext" class="ui-btn-left">Home</a>', $buffer); 
		$buffer = str_replace('##EXIT_BTN##', '<a id="exit" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Home</a>', $buffer);			
		$buffer = str_replace('##HEADER##', $menu_inner_tmp, $buffer); 
		$buffer = str_replace('##TITLE_CONTENT##', '', $buffer);			
		$buffer = str_replace('##FOOTER##', '', $buffer);

		$biblio_id = get_oldest_grandpa($parent_id);

		$buffer = str_replace('##BIBLIO_CAT_ID##', $biblio_id, $buffer);

		$biblio = DB::query_single("SELECT * FROM categoria WHERE cat_id = $biblio_id");

		$biblio_file_name = 'biblio_' . $biblio_id . '.html';
		$biblio_dir_pages = Cfg::get_param('dir_pages'); 
		$biblio_file_path = $biblio_dir_pages . $biblio_file_name; 

		$buffer_biblio =  str_replace('##HEADER##', $biblio->nombre, $popup_template);
		$buffer_biblio =  str_replace('##CONTENT##', $biblio->bibliografia, $buffer_biblio);

		file_put_contents($biblio_file_path, $buffer_biblio); 
	}

	$lis = '';
	$popups = '';
	for ( $i = 0 ; $i < count ($list) ; $i++ )
	{
		$rec = $list[$i]; 
		if ( trim($rec->texto) == '' )
		{
			switch ($rec->cat_id) 
			{
				case 1: 
					$li_href = 'endocarditis.html';
				break;

				default:  
					$li_href = 'page_' . $rec->cat_id . '.html';
				break;
			}

			$lis .= '<li data-icon="false"><a href="' . $li_href . '" data-transition="slide" >' . $rec->nombre . '</a></li>' . "\n";				
		} else 
		{
			$lis .= '<li data-icon="false"><a href="page_' . $rec->cat_id . '.html" data-rel="dialog"  data-transition="none">' . $rec->nombre . '</a></li>'. "\n";
		}
	}

	$buffer = str_replace('##CONTENT_LIs##', $lis, $buffer);
	$buffer = str_replace('##POPUPS##', $popups, $buffer);
	
	

	file_put_contents($file_path, $buffer); 
}

function create_popup($cat_id)
{
	global $popup_template ; 
	
	$file_name = 'page_' . $cat_id . '.html';
	$dir_pages = Cfg::get_param('dir_pages'); 
	$file_path = $dir_pages . $file_name; 
	
	$rec = DB::query_single("SELECT * FROM categoria WHERE cat_id = ? ", array($cat_id));

	$buffer =  str_replace('##HEADER##', $rec->nombre, $popup_template);
	$buffer =  str_replace('##CONTENT##', $rec->texto, $buffer);

	file_put_contents($file_path, $buffer); 
}

function get_oldest_grandpa($cat_id)
{
	$sql = "SELECT cat_id, parent_id FROM categoria WHERE cat_id = ? ";

	$rec = DB::query_single($sql, array($cat_id));
	if ($rec->parent_id == 0 ) return $cat_id;
	
	while ($rec->parent_id != 0) 
	{ 
		$rec = DB::query_single($sql, array($rec->parent_id)); 
		if ($rec->parent_id == 0) break;
	}
	
	return $rec->cat_id;
}

function build_files () 
{
	$list = DB::query_list("SELECT DISTINCT parent_id FROM categoria  ORDER BY nombre");
	
	for ( $i  = 0 ; $i < count($list) ; ++$i)
	{
		$rec = $list[$i]; 
		echo "CREANDO NODO : {$rec->parent_id} <BR />" ; 
		search_nodes($rec->parent_id);
	}
	$list = array(); 
	$list = DB::query_list("SELECT cat_id FROM categoria WHERE texto is not null AND trim(texto) <> '' ORDER BY cat_id");
	
	for ( $i  = 0 ; $i < count($list) ; ++$i)
	{
		$rec = $list[$i]; 
		echo "CREANDO popup : {$rec->cat_id} <BR />" ; 
		create_popup($rec->cat_id);
	}
}

build_files(); 
